import mysql.connector
import csv

def create_table_matches(myCurser):
    myCurser.execute("CREATE TABLE IF NOT EXISTS matches ("
                      "id int PRIMARY KEY, season int, city varchar(50), "
                      "date varchar(15), team1 varchar(50), "
                      "team2 varchar(50), toss_winner varchar(50), "
                      "toss_decision varchar(50), result varchar(50), "
                      "dl_applied varchar(50), winner varchar(50), "
                      "win_by_runs int, win_by_wickets int, "
                      "player_of_match varchar(50), venue varchar(250), "
                      "umpire1 varchar(50), umpire2 varchar(50), umpire3 varchar(50))")

def insert_into_matches(myCurser):
    sql = 'INSERT INTO matches (id,season,city, date,team1,team2,toss_winner, toss_decision, result, dl_applied,winner, win_by_runs, win_by_wickets,player_of_match,venue, umpire1,umpire2,umpire3) VALUES("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")'
    data = []
    with open ('matches.csv') as csvfile:
        matches_reader = csv.reader(csvfile)
        firstline = True
        for match in matches_reader:
            if firstline:
                firstline = False
                continue
            t = row
            t[0] = int(t[0])
            t[1] = int(t[1])
            t[11] = int(t[11])
            t[12] = int(t[12])
            data.append(tuple(t))
    myCurser.executemany(sql,data)


my_db = mysql.connector.connect(host= 'localhost',
                                user = 'ipl_user',
                                passwd ='karan',
                                database = 'IPL',
                                auth_plugin = 'mysql_native_password')

mycurser = my_db.cursor()
create_table_matches(mycurser)

insert_into_matches(mycurser)

my_db.commit()


def create_table_deliveries(mycurser):
    mycurser.execute("CREATE TABLE IF NOT EXISTS deliveries " 
                    "(match_id int, inning int,batting_team varchar(50)," 
                    " bowling_team varchar(50), over_ int,ball int, " 
                    "batsman varchar(50), non_striker varchar(50), " 
                    "bowler varchar(50), is_super_over int, " 
                    "wide_runs int, bye_runs int, legbye_runs int, " 
                    "noball_runs int, penalty_runs int, batsman_runs int, "
                    "extra_runs int, total_runs int, player_dismissed varchar(50), "
                    "dismissal_kind varchar(50), fielder varchar(50), FOREIGN KEY (match_id) REFERENCES matches(id))")

def insert_into_deliveries(mycurser):
    sql = 'INSERT INTO deliveries (match_id, inning, batting_team ,bowling_team , over_,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder) VALUES ("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s")'
    data = []
    with open ('deliveries.csv') as csvfile:
        deliveries_reader = csv.reader(csvfile)
        firstline = True
        for delivery in deliveries_reader:
            if firstline:
                firstline = False
                continue
            t = row
            t[0] = int(t[0])
            t[1] = int(t[1])
            t[4] = int(t[4])
            t[5] = int(t[5])
            t[9] = int(t[9])
            t[10] = int(t[10])
            t[11] = int(t[11])
            t[12] = int(t[12])
            t[13] = int(t[13])
            t[14] = int(t[14])
            t[15] = int(t[15])
            t[16] = int(t[16])
            t[17] = int(t[17])
            print(tuple(t))
            data.append(tuple(t))
    myCourser.executemany(sql,data)
    my_db.commit()

create_table_deliveries(mycurser)

insert_into_deliveries(mycurser)

