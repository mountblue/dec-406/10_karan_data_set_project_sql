import mysql.connector

def connect_to_database():
    my_db = mysql.connector.connect(host= 'localhost',
                                user = 'ipl_user',
                                passwd ='karan',
                                database = 'IPL',
                                auth_plugin = 'mysql_native_password')
    return my_db

#task3 For the year 2016 plot the extra runs conceded per team.


def extra_runs_in2016():
    ipl_db = connect_to_database()
    mycurser = ipl_db.cursor()
    mycurser.execute("SELECT bowling_team, SUM(extra_runs) FROM deliveries INNER JOIN matches on matches.id = deliveries.match_id WHERE season= 2016 group by bowling_team")
    matches_won_every_year = mycurser.fetchall()
    return matches_won_every_year

print(extra_runs_in2016())