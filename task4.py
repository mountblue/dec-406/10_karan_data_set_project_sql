import mysql.connector

def connect_to_database():
    my_db = mysql.connector.connect(host= 'localhost',
                                user = 'ipl_user',
                                passwd ='karan',
                                database = 'IPL',
                                auth_plugin = 'mysql_native_password')
    return my_db

# For the year 2015 plot the top economical bowlers.

def economical_bowlers():
    ipl_db = connect_to_database()
    mycurser = ipl_db.cursor()
    mycurser.execute("SELECT bowler, ROUND(SUM(total_runs-bye_runs)*6/COUNT(extra_runs),2) AS economies FROM deliveries INNER JOIN matches ON matches.id = deliveries.match_id WHERE season = 2015 group by deliveries.bowler order by economies")
    economical_bowlers = mycurser.fetchall()
    return economical_bowlers

print(economical_bowlers())